import java.util.ArrayList;

public class SudokuContainer {
	private static final int MAX_STORED_SOLUTIONS = 750;
	
	private ArrayList<Board> solutions;
	private int solutionCount = 0;
	
	public SudokuContainer() {
		solutions = new ArrayList<>();
	}
	
	public void insert(Board board) {
		solutionCount++;
		/* Don't add more boards if there are already 750 */
		if (solutions.size() <= MAX_STORED_SOLUTIONS) {
			solutions.add(board);
		}
	}
	
	public ArrayList<Board> get() { 
		return solutions; 
	}
	
	public int getSolutionCount() { 
		return solutionCount;
	}
	
	public void clear() {
		solutionCount = 0;
		solutions.clear();
	}
}