import java.io.File;

public class SudokuModel {
	private File file;
	private Board board;
	private SudokuContainer solutions = new SudokuContainer();
	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void solve() {
		board = new Board(file);
		board.setSolutionContainer(solutions);
		board.getSolutionContainer().clear();
		board.getSquares()[0][0].fillInnRemainingOfBoard();
	}

	public Board getBoard() {
		return board;
	}

	public SudokuContainer getSolutions() {
		return solutions;
	}
}
