import java.util.ArrayList;

abstract public class Square {
	private int value;
	private Square next = null;
	private Box box;
	private Column column;
	private Row row;
	private Board board;
	
	public int getValue() {
		return this.value;
	}
	
	public void fillInnRemainingOfBoard() {												
		/* for empty squares */
		if ( this.value == 0) {
			for (Integer i : this.getPossibleMoves()) {	//find possible numbers			
				//assign value
				this.setValue(i);
				
				if(next != null) {
					next.fillInnRemainingOfBoard();
				} else {
					//System.out.println(this.board);
					board.getSolutionContainer().insert(new Board(this.board.toString()));	
				}
			}
			
			this.setValue(0);
		} else {
			if(next == null) {
				board.getSolutionContainer().insert(new Board(this.board.toString()));	
			} else {
				next.fillInnRemainingOfBoard();
			}
		}
	}
	
	public ArrayList<Integer> getPossibleMoves() {
		ArrayList<Integer> options = new ArrayList<Integer>(); 
		
		for (int i = 1; i <= board.getSize(); i++) {
			/* Eliminate numbers that would not be legal to place */
			if (!column.contains(i) && !row.contains(i) && !box.contains(i)) {
				options.add(i);
			}
		}
		
		return options;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}
	
	public Board getBoard() {
		return this.board;
	}
	
	public void setColumn(Column column) {
		this.column = column;	
	}

	public void setRow(Row row) {
		this.row = row;
	}	
		
	public Box getBox() { 
		return box; 
	}
	
	public Row getRow() { 
		return row; 
	}
	
	public Column getColumn() { 
		return column; 	
	}
	
	public void setValue(int value) {
		this.value = value;
		
	}

	public Square getNext() {
		return this.next;
	}

	public void setNext(Square next) {
		this.next = next;
	}

	public void setBox(Box box) {
		this.box = box;
	}
}