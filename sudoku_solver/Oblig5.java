import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Oblig5 {
	
	public static String IN_FILE = null;
	public static String OUT_FILE = null;

	private static SudokuView view = null;
	private static SudokuModel model = null;
	
	public static void main(String[] args) {

		
		if(args.length > 0) {
			IN_FILE = args[0];
			
			if(args.length > 1) {
				OUT_FILE = args[1];
			}
		}
		
		if(IN_FILE != null && OUT_FILE != null) {
			model = new SudokuModel();
			model.setFile(new File(IN_FILE));
			model.solve();
			writeSolutionsToFile(OUT_FILE);
		} else {
			view = new SudokuView();
			model = new SudokuModel();
			new SudokuController(view, model);
		}
		
	}

	private static void writeSolutionsToFile(String file) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)));){
			ArrayList<Board> list = model.getSolutions().get();
			int count = 1;
			
			for(Board b : list) {
				bw.write(count + ": ");
				Row[] rows = b.getRows();
				
				for(int i = 0; i < rows.length; i++) {
					bw.write(rows[i] + "//");
				}
				bw.write('\n');
				count++;
			}
		} catch (IOException e) {
			System.out.println("Something went wrong during file IO");
			System.exit(1);
		}
		
	}

}
