public abstract class Unit {
	private Square[] squares;
	
	public Unit(int size) {
		squares = new Square[size];
	}
	
	public void add(Square sq) {
		for (int i = 0; i < squares.length; i++) {
			if (squares[i] == null) {
				squares[i] = sq;
				return;
			}
		}
		System.err.println("no space in " + squares);
	}
	
	public boolean contains(int value) {
		for (int i = 0; i < squares.length; i++) {
			if (squares[i].getValue() == value) {
				return true;
			}
		}
		return false;
	}
	
	public Square[] getSquares() {
		return this.squares;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < squares.length; i++) {
			if(squares[i].getValue() > 9) {
				builder.append(Character.toChars(squares[i].getValue() + 55));
			} else {
				builder.append(squares[i].getValue());
			}
		}
		
		return builder.toString();
	}
 }
