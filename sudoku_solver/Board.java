import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

public class Board {
	
	private SudokuContainer solutionContainer;
	private Square[][] squares;
	private Column[] columns;
	private Row[] rows;
	private Box[] boxes;
	
	private int boxWidth;
	private int boxHeight;
	private int size;
	
	private ArrayList<Square> parsedValues;
	
	public Board(File file) {
		if(file.exists()) {
			//parse
			try {
				parseFile(new FileReader(file));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			size = boxHeight * boxWidth;
			
			initUnits();
			parseBoard(squares);
		} else {
			System.out.println("File not found");
			System.exit(1);
		}
	}
	
	public Board(String string) {
		//parse
		parseFile(new StringReader(string));
		size = boxHeight * boxWidth;
		initUnits();
		parseBoard(squares);
	}

	private void parseBoard(Square[][] squares) {
		int count = 0;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				
				squares[i][j] = parsedValues.get(count);
				count++;
				
				squares[i][j].setBoard(this);
				
				//Add squares to boxes and colums in the board
				columns[i].add(squares[i][j]);
				rows[j].add(squares[i][j]);
				
				
				//Associate columns and rows with squares
				squares[i][j].setColumn(columns[i]);
				squares[i][j].setRow(rows[j]);
				
				
				final int index = (j / boxWidth) + ((i / boxHeight) * boxHeight);
				
				//same for boxes
				squares[i][j].setBox(boxes[index]);
				boxes[index].add(squares[i][j]);
				
				
				addToLink(squares[i][j]); //create linked list					
			}
		}
	}
	
	
	private void parseFile(Reader board) {
		parsedValues = new ArrayList<Square>(); 
		
		try (BufferedReader reader = new BufferedReader(board)){
			
			boxHeight = Character.getNumericValue(reader.readLine().charAt(0));
			boxWidth = Character.getNumericValue(reader.readLine().charAt(0));
			
			String line = null;
			while ((line = reader.readLine()) != null) {
				for (int i = 0;i < line.length(); i++){
					if (line.charAt(i) == '.') {
						parsedValues.add(new OpenSquare());
					} else if (!(line.charAt(i) == '\n')) {
						parsedValues.add(new LockedSquare(Character.getNumericValue(line.charAt(i))));
					}
				}
			}
			
		} catch (IOException ex) {
			System.out.println("Something went wrong during file I/O, terminating...");
			System.exit(-1);
		} catch (NumberFormatException ex) {
			System.out.println("The structure of the file is incorrect, terminating...");
			ex.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void initUnits() {
		squares = new Square[size][size];
		columns = new Column[size];
		rows = new Row[size];
		boxes = new Box[size];
		
		for(int i = 0; i < size; i++) {
			columns[i] = new Column(size);
			rows[i] = new Row(size);
			boxes[i] = new Box(size);
		}
	}
			
	public Box[] getBoxes() {
		return this.boxes;
	}
	
	public Square[][] getSquares() {
		return this.squares;
	}
	
	private void addToLink(Square square) {	
		Square tmp = squares[0][0];
	   	while (tmp.getNext() != null && tmp != tmp.getNext()) {
	   		tmp = tmp.getNext();
    	}
    	
    	tmp.setNext(square);
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(boxWidth + "\n");
		builder.append(boxHeight + "\n");
		
		for (int i = 0; i < columns.length; i++) {
			for (int j = 0; j < rows.length; j++) {
				if(squares[i][j].getValue() > 9) {
					builder.append(Character.toChars(squares[i][j].getValue() + 55));
				} else {
					builder.append(squares[i][j].getValue() + "");
				}
			}
			builder.append("\n");
		}

		return builder.toString();
	}

	public Row[] getRows() {
		return this.rows;
	}
	
	public int getSize() {
		return this.size;
	}

	public SudokuContainer getSolutionContainer() {
		return solutionContainer;
	}

	public void setSolutionContainer(SudokuContainer solutionContainer) {
		this.solutionContainer = solutionContainer;
	}
}
