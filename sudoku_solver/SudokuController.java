
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


public class SudokuController {
	private SudokuView view;
	private SudokuModel model;
	
	SudokuController(SudokuView view, SudokuModel model) {
		this.view = view;
		this.model = model;
		
		this.view.addListeners(new ViewListener());;
	}
	
	private JPanel getJPanelRepresentation(Board board) {
		
		Square[][] squares = board.getSquares();
		
		JPanel tmp = new JPanel();
		tmp.setLayout(new GridLayout(squares.length, squares[0].length));
		
		for (int row = 0; row < squares.length; row++) {
			for (int column = 0; column < squares[row].length; column++) {
				JLabel label = new JLabel(Integer.toString(squares[row][column].getValue()));
				label.setBorder(BorderFactory.createLineBorder(Color.black));
			    label.setHorizontalAlignment( SwingConstants.CENTER);
				tmp.add(label);
			}

		}
		
		return tmp;
	}
	

	
	public class ViewListener implements ActionListener {
		int currentPosition = 0;
		
		public ViewListener() {
			if(Oblig5.IN_FILE != null) {
				model.setFile(new File(Oblig5.IN_FILE));			
				model.solve();
				String solutionCount = Integer.toString(model.getSolutions().getSolutionCount());
				view.getSolveButton().setEnabled(false);
				view.getSolutionText().setText(solutionCount + " solution(s)");
				updateBoard();
			}
			
			checkPosition();
		}
		
		private void checkPosition() {
			if ((currentPosition+1) >= (model.getSolutions().getSolutionCount())) {
				view.getNextButton().setEnabled(false);
			} else {
				view.getNextButton().setEnabled(true);
			}
			
			if (currentPosition-1 == -1) {
				view.getPreviousButton().setEnabled(false);
			} else {
				view.getPreviousButton().setEnabled(true);
			}
		}
		
		private void updateBoard() {
			ArrayList<Board> solutions = model.getSolutions().get();
			Board board = solutions.get(currentPosition);	
			BorderLayout layout = (BorderLayout) view.getRootPanel().getLayout();
			JPanel panel = getJPanelRepresentation(board);
			
			view.getRootPanel().remove(layout.getLayoutComponent(BorderLayout.CENTER));
			view.getRootPanel().add(panel, BorderLayout.CENTER);
			view.getRootPanel().updateUI();
		}
		
		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == view.getOpenFileButton()) {
				JFileChooser fc = new JFileChooser(".");
				int returnVal = fc.showOpenDialog(view.getOpenFileButton());
					
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					model.setFile(fc.getSelectedFile());
					if (!view.getSolveButton().isEnabled()) {
						view.getSolveButton().setEnabled(true);
					}
				}
			} else if (evt.getSource() == view.getNextButton()) {
				/* If pressed 'next' button */
				currentPosition++;
				updateBoard();
				checkPosition();
				
			} else if (evt.getSource() == (view.getPreviousButton())) {
				/* If pressed 'previous' button */		
				currentPosition--;
				updateBoard();
				checkPosition();
			} else if (evt.getSource() == view.getSolveButton()) {
				/* If pressed solveButton */
				if (model.getFile() != null) {
					
					model.solve();
					String solutionCount = Integer.toString(model.getSolutions().getSolutionCount());
					view.getSolveButton().setEnabled(false);
					view.getSolutionText().setText(solutionCount + " solution(s)");
					
					updateBoard();
					checkPosition();
				}
			}
			
			
		} 
	}
		
}
