import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class SudokuView {
	private JFrame frame;
	private JPanel rootPanel;
	private JPanel gridPanel;
	private JLabel solutionLabel;
	
	public JPanel getGridPanel() {
		return gridPanel;
	}

	private JButton openFileButton, solveButton;
	private JButton nextButton, previousButton;
	
	public SudokuView() {
		SwingUtilities.invokeLater(new Runnable() { 
			public void run() {
				frame = new JFrame("Sudoku Solver");
			}
		});
		
		
		rootPanel = new JPanel();
		gridPanel = new JPanel();
		
		openFileButton = new JButton("Open file");
		solveButton = new JButton("Solve board");
		nextButton = new JButton("Next solution");
		previousButton = new JButton("Previous solution");
		
		solutionLabel = new JLabel("0 Solution(s)");
		
		solveButton.setEnabled(false);
		
		rootPanel.setLayout(new BorderLayout());
		//gridPanel.setLayout(new GridLayout(9,9));
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout());
		northPanel.add(openFileButton);
		northPanel.add(solveButton);
		
		rootPanel.add(northPanel , BorderLayout.NORTH);
		
		rootPanel.add(gridPanel, BorderLayout.CENTER);
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new FlowLayout());
		southPanel.add(previousButton);
		southPanel.add(nextButton);
		southPanel.add(solutionLabel);
		
		
		rootPanel.add(southPanel, BorderLayout.SOUTH);
		
		frame.add(rootPanel);
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	void addListeners(ActionListener listener) {
		nextButton.addActionListener(listener);
		previousButton.addActionListener(listener);
		openFileButton.addActionListener(listener);
		solveButton.addActionListener(listener);
	}

	public JButton getNextButton() {
		return nextButton;

	}
	
	public JButton getPreviousButton() {
		return previousButton;
	}
	
	public JButton getOpenFileButton() {
		return openFileButton;
		
	}
	
	public JButton getSolveButton() {
		return solveButton;
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setGridPanel(JPanel panel) {
		this.gridPanel = panel;
		
	}
	
	public JLabel getSolutionText() {
		return this.solutionLabel;
	}

	public JPanel getRootPanel() {
		return this.rootPanel;
	}
	
}
