#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>

#include "./protocol.h"
#include "./communication.h"
#include "./navigator.h"


char* list_dir(char *str, char *filelist) {
    DIR* cdir = opendir(str);

    if (cdir == NULL) return NULL;

    struct dirent* dent = NULL;
    char bigbuffer[1000];
    bigbuffer[0] = 0;
	   
    while ((dent = readdir(cdir)) != NULL) {
        strcat(bigbuffer, dent->d_name);
        strcat(bigbuffer, "\n");
    }
        
    strcpy(filelist, bigbuffer);
 
    closedir(cdir);
    return filelist;
}

int status(char *str) {
    char filename[256];

    memcpy(filename, &str[5], 255);
    filename[255] = '\0';

    struct stat st;

    if(lstat(filename, &st) != -1) {
        if (S_ISREG(st.st_mode)) {
            return ISREG;

        } else if (S_ISDIR(st.st_mode)) {
            return ISDIR;

        } else if (S_ISLNK(st.st_mode)) {
            return ISLINK;

        } else {
            return SPECIAL;

        }
    }

    return INV;
}

int change_dir(char *msg, char *path) {
    struct stat st;

    char next[MAXPATHLEN];
    memcpy(next, &msg[3], sizeof(next));

    if(next[MAXPATHLEN-1] == '\n') {
        next[MAXPATHLEN-1] = '\0';
    }

    if (lstat(next, &st) == -1) {
        return INV;
    }

    if (!S_ISDIR(st.st_mode)) {
	return INV;
    }

    chdir(path);
    chdir(next);

    char tmp[MAXPATHLEN];
    getcwd(tmp, sizeof(tmp)); //get fullpath

    chdir(root); //back to root

    if (strncmp(root, tmp, strlen(root)) == 0) {
        strncpy(path, tmp, MAXPATHLEN);
        return OK;
    } else {
        return NO;
    }
}

char *print_file(char *msg, char *result) {
    FILE* fp;

    fp = fopen(msg, "r");

    char c;

    for (int i = 0; ((c = fgetc(fp)) != EOF); i++) {
        if (isprint(c)) {
            result[i] = c;
        } else {
            result[i] = '.';
        }
    }

    fclose(fp);
    return result;
}
