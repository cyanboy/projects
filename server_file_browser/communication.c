#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <signal.h>
#include <netdb.h>

#include "./communication.h"
#include "./navigator.h"
#include "./protocol.h"


char root[MAXPATHLEN];
int host_fd;
int client_fd;
struct sockaddr_in host_info;
fd_set fds;
fd_set read_fds;

client_t clients[FD_SETSIZE];

int init_server_com(int port) {
    host_fd = socket(AF_INET, SOCK_STREAM, 0);
    getcwd(root, sizeof(root));
    memset(&host_info, 0, sizeof(host_info));

    int activate = 1;
    setsockopt(host_fd, SOL_SOCKET, SO_REUSEADDR, &activate, sizeof(int));

    host_info.sin_family = AF_INET;
    host_info.sin_port = htons(port);
    host_info.sin_addr.s_addr = INADDR_ANY;

    if ((bind(host_fd, (struct sockaddr*) &host_info, sizeof(host_info))) != 0) {  // bind
        perror("bind");
        return 0;
    }

    if ((listen(host_fd, 5)) != 0) {  //listen
        perror("listen");
        return 0;
    }

    FD_ZERO(&fds);
    FD_SET(host_fd, &fds);

    for (int i = 0; i < FD_SETSIZE; ++i) {
        strcpy(clients[i].dir, root);
        clients[i].fd = i;
    }

    return 1;
}

void accept_client() {
    int client_fd = accept(host_fd, NULL, NULL);

    if (client_fd < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    FD_SET(client_fd, &fds);
}

void handle_client(client_t *c) {
    char buffer[2048];
    ssize_t bytes = recv(c->fd, buffer, sizeof(buffer), 0);

    if (bytes < 0) {
        perror("recv");
        exit(EXIT_FAILURE);
    } else if (bytes == 0) {
        close(c->fd);
        FD_CLR(c->fd, &fds);

        fprintf(stderr, "A client has disconnected\n");

    } else {
        buffer[bytes] = 0;
        int code = parseMessage(buffer);
        handleMessage(c, code, buffer);
    }

}

void handle_server_com() {
    while (1) {
        read_fds = fds;

        if (select(FD_SETSIZE, &read_fds, NULL, NULL, NULL) < 0) {
            perror("select");
            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < FD_SETSIZE; ++i) {
            if (FD_ISSET(i, &read_fds)) {
                if (clients[i].fd == host_fd) {
                    accept_client();
                } else {
                    handle_client(&clients[i]);
                }
            }
        }
    }
}

void close_server_com() {
    signal(2, close_server_com);

    //close all clients ?
    /* for (int i = 0; i < FD_SETSIZE; ++i) { */
    /*     if (FD_ISSET(i, &read_fds)) { */
    /*         if (i != host_fd) { */
    /*             close(i); */
    /*             FD_CLR(i, &fds); */
    /*         } */
    /*     } */
    /* } */

    close(host_fd);
    exit(0);
}

int init_client_com(char *argv[]) {
    struct in_addr addr;  // host
    memset(&addr, 0, sizeof(addr));

    struct addrinfo hints, *res;
    int status;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if ((status = getaddrinfo(argv[1], argv[2], &hints, &res)) != 0) {
        perror("getaddrinfo");
        exit(1);
    }

    struct addrinfo *cur = res;

    client_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (client_fd == -1) {
        perror("socket");
        return -2;
    }

    status = connect(client_fd, cur->ai_addr, cur->ai_addrlen);

    if (status != 0) {
        perror("connect");
        return -2;
    }

    freeaddrinfo(res);
    return 1;
}

void handle_client_com() {
    fprintf(stdout, "Welcome to the home-ex2 1.0\nType ? to list commands\n");
    char buffer[10000];
    // memset(buffer, 0, sizeof(buffer));
    while (1) {

        fprintf(stdout, "home-ex2> ");

        fgets(buffer, sizeof(buffer), stdin);

        buffer[strlen(buffer) - 1] = '\0'; //remove newline

        if (strcmp(buffer, "?") == 0) {
            fprintf(stdout, "\tLS: list directory\n\tPWD: print working directory\n");
            fprintf(stdout, "\tCD: change directory\n\tSTAT: print file information\n");
            fprintf(stdout, "\tCAT: print file\n\tEXIT: close the program\n");
        } else if (strcmp(buffer, "EXIT") == 0) {
            close_client_com();
        } else if (strcmp(buffer, "") == 0) {
            ;
        }else {
            send(client_fd, buffer, strlen(buffer) + 1, 0);

            memset(buffer, 0, sizeof(buffer));

            recv(client_fd, buffer, sizeof(buffer), 0);

            int code = parseMessage(buffer);

            switch (code) {
            case NO:
                fprintf(stdout, "You don't have permission to do this\n");
                break;
            case OK:
                //success
                break;
            case INV:
                fprintf(stdout, "Invalid parameter\n");
                break;
            case ISREG:
                fprintf(stdout, "It's a regular file\n");
                break;
            case ISDIR:
                fprintf(stdout, "It's a directory\n");
                break;
            case ISLINK:
                fprintf(stdout, "It's a link\n");
                break;
            case SPECIAL:
                fprintf(stdout, "It's special\n");
                break;
            case UNKNOWN:
                fprintf(stdout, "Unknown command\n");
                break;
            case DATA:
                fprintf(stdout, "%s\n", buffer);
            }

            fflush(stdin);
        }
    }

}

void close_client_com() {
    signal(2, close_client_com);
    close(client_fd);
    exit(0);
}
