#ifndef PROTOCOL_H_
#define PROTOCOL_H_
#include "communication.h"

enum STATUS {
    LS,
    PWD,
    CD,
    NO,
    OK,
    CAT,
    STAT,
    INV,
    ISREG,
    ISDIR,
    ISLINK,
    SPECIAL,
    DATA,
    UNKNOWN
};

int parseMessage(char *msg);
void handleMessage(client_t *client, int code, char *msg);

#endif
