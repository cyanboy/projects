// Copyright 2014 <328>

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "./communication.h"

int main(int argc, char *argv[]) {
    signal(SIGINT, close_server_com);

    if (argc != 2) {  // check params
        printf("usage ./server <port>\n");
        return -1;
    }

    init_server_com(atoi(argv[1]));
    handle_server_com();
    
    return 0;
}
