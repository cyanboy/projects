#include <signal.h>
#include <stdio.h>
#include "./communication.h"

int main(int argc, char *argv[]) {
    signal(2, close_client_com);

    if (argc != 3) {
        printf("usage ./client <ip> <port>\n");
        return 1;
    }

    init_client_com(argv);

    handle_client_com();
   
    return 0;
}
