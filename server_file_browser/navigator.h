#ifndef NAVIGATOR_H_
#define NAVIGATOR_H_
#include "communication.h"

char *list_dir(char *str, char * filelist);
int change_dir(char *msg, char *path);
int status(char *str);
char *print_file(char *msg, char *result);

#endif  // NAVIGATOR_H_
