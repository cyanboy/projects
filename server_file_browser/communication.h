/* Copyright 2014 <328>*/

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_
#include <sys/param.h>

typedef struct {
    int fd;
    char dir[MAXPATHLEN];
} client_t;



int init_server_com(int port);
void handle_server_com();
void accept_client();
void handle_client(client_t *c);
void close_server_com();

extern char root[MAXPATHLEN];
extern int host_fd;
extern int client_fd;

extern struct sockaddr_in host_info;
//extern fd_set fds;
//extern fd_set read_fds;

int init_client_com();
void handle_client_com();
void close_client_com();

#endif 
