#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include "./navigator.h"
#include "./protocol.h"


int parseMessage(char *msg) {
    if (strcmp("PWD", msg) == 0) {
        return PWD;
    }

    if (strcmp("LS", msg) == 0) {
        return LS;
    }

    if (strncmp("CD ", msg, 3) == 0) {
        return CD;
    }

    if (strncmp("CAT ", msg, 3) == 0) {
        return CAT;
    }

    if (strncmp("STAT ", msg, 5) == 0) {
        return STAT;
    }

    if (strcmp("OK", msg) == 0) {
        return OK;
    }

    if (strcmp("NO", msg) == 0) {
        return NO;
    }

    if (strcmp("ISREG", msg) == 0) {
        return ISREG;
    }

    if (strcmp("ISDIR", msg) == 0) {
        return ISDIR;
    }

    if (strcmp("ISLNK", msg) == 0) {
        return ISLINK;
    }

    if (strcmp("INV", msg) == 0) {
        return INV;
    }

    if (strcmp("SPECIAL", msg) == 0) {
        return SPECIAL;
    }

    if (strcmp("UNKNOWN", msg) == 0) {
        return UNKNOWN;
    }

    return DATA;
}

void handleMessage(client_t *client, int code, char *msg) {


    if (code == LS) {
        char ldir[10000];
        list_dir(client->dir, ldir);
        send(client->fd, ldir, strlen(ldir) + 1, 0);
    } else if (code == PWD) {
        send(client->fd, client->dir, sizeof(client->dir), 0);

    } else if (code == CD) {
        int s = change_dir(msg, client->dir);

        switch(s) {
        case OK:
            send(client->fd, "OK", 3, 0);
            break;
        case NO:
            send(client->fd, "NO", 3, 0);
            break;
        case INV:
            send(client->fd, "INV", 4, 0);
            break;
        default: //should never happen
            exit(EXIT_FAILURE);
        }

    } else if (code == STAT) {
        int s = status(msg);

        if (s == ISREG) {
            send(client->fd, "ISREG", 6, 0);
        } else if (s == ISDIR) {
            send(client->fd, "ISDIR", 6, 0);
        } else if (s == ISLINK) {
            send(client->fd, "ISLNK", 6, 0);
        } else if (s == INV) {
            send(client->fd, "INV", 5, 0);
        } else {
            send(client->fd, "SPECIAL", 8, 0);
        }

    } else if (code == CAT){

        char filename[256];

        memcpy(filename, &msg[4], 255);
        filename[255] = '\0';

        struct stat st;

        int size;
        if(lstat(filename, &st) != -1) {
            size = st.st_size;
            char filebuffer[size];
            print_file(filename, filebuffer);
            //fprintf(stderr, "%s", filebuffer);
            send(client->fd, filebuffer, sizeof(filebuffer), 0);
        } else {
            send(client->fd, "INV", 4, 0);
        }

    } else {
        send(client->fd, "UNKNOWN", 8, 0);
    }


}
