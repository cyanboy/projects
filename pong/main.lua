function love.load()
   love.graphics.setBackgroundColor(0, 0, 0)
   love.mouse.setVisible(false)
   sound = love.audio.newSource("boop.wav", "static")
   ping = love.audio.newSource("ping.wav", "static")
   die = love.audio.newSource("pain.wav", "static")
   theme = love.audio.newSource("theme.ogg")
   theme:setLooping(true)
   theme:play()

   local height = 16

   speed = 300

   player1 = { height = 64,
               width = 16,
               y = love.graphics.getHeight() / 2 - height / 2,
               x = 16,
               score = 0}

   player2 = { height = 64,
               width = 16,
               y = love.graphics.getHeight() / 2 - height / 2,
               x = love.graphics.getWidth() - 32,
               score = 0}

   ball = { height = 16,
            width = 16,
            x = love.graphics.getWidth() / 2,
            y = love.graphics.getHeight() / 2,
            vx = 100,
            vy = -150 }

   love.graphics.setNewFont(72)

end

function love.update(dt)
   if love.keyboard.isDown("w") then
      player1.y = player1.y - (speed * dt)
      if player1.y < 0 then player1.y = 0 end
   elseif love.keyboard.isDown("s") then
      player1.y = player1.y + (speed * dt)
      if player1.y > love.graphics.getHeight() - player1.height then player1.y = love.graphics.getHeight() - player1.height end
   end

   if love.keyboard.isDown("up") then
      player2.y = player2.y - (speed * dt)
      if player2.y < 0 then player2.y = 0 end
   elseif love.keyboard.isDown("down") then
      player2.y = player2.y + (speed * dt)
      if player2.y > love.graphics.getHeight() - player2.height then player2.y = love.graphics.getHeight() - player2.height end
   end


   if ball.x <= player1.width and
      (ball.y + ball.height) >= player1.y and
      ball.y < (player1.y + player1.height)
   then
      ball.vx = math.abs(ball.vx)
   end

   if ball.y < 0 then --bounce top
      ball.vy = math.abs(ball.vy)
      ping:play()
   end

   if ball.y > love.graphics.getHeight() - ball.height then --bounce bottom
      ball.vy = -math.abs(ball.vy)
      ping:play()
   end


   if CheckCollision(ball.x, ball.y, ball.width, ball.height, player1.x, player1.y, player1.width, player1.height) then
      ball.vx = math.abs(ball.vx * 1.10)
      sound:play()
   end

   if CheckCollision(ball.x, ball.y, ball.width, ball.height, player2.x, player2.y, player2.width, player2.height) then
      ball.vx = -math.abs(ball.vx * 1.10)
      sound:play()
   end

   if ball.x < 0 then -- off screen left

      ball.x = love.window.getWidth() / 2
      ball.y = love.window.getHeight() / 2

      ball.vx = 100
      ball.vy = -100

      player2.score = player2.score + 1

      die:play()
   end

   if ball.x > love.graphics.getWidth() - ball.width then -- off screen right

      ball.x = love.graphics.getWidth() / 2
      ball.y = love.graphics.getHeight() / 2

      ball.vx = -100
      ball.vy = -100

      player1.score = player1.score + 1
      die:play()
   end

   ball.x = ball.x + ball.vx * dt
   ball.y = ball.y + ball.vy * dt
end

function love.draw()


   love.graphics.setColor(0, 255, 255) -- player2_yan
   love.graphics.rectangle("fill", player1.x, player1.y, player1.width, player1.height)

   love.graphics.setColor(255, 0, 255) -- magenta
   love.graphics.rectangle("fill", player2.x, player2.y, player2.width, player2.height)

   love.graphics.setColor(255, 255, 0) -- yellow
   love.graphics.rectangle("fill", ball.x, ball.y, ball.width, ball.height)

   love.graphics.setColor(255, 255, 255)

   local score_txt = player1.score .. " - " .. player2.score
   local txt_width = love.graphics.getFont():getWidth(score_txt)

   love.graphics.print(score_txt, love.graphics.getWidth() / 2 - txt_width / 2, 16)
end

function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2)
   return x1 < x2+w2 and
      x2 < x1+w1 and
      y1 < y2+h2 and
      y2 < y1+h1
end
