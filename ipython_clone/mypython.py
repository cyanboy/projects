import sys
from feedline import feedline, history
from getchar import getchar

count = 0

def prompt():
    """Reads a line of input, until newline is encountered and 
       passes the line to the function feedline
    
    Returns:
        None

    Use arrow keys to cycle through command history
    Use Ctrl-D to exit program
    """ 
    
    global count
    index = 0
    sys.stdout.write("In [{}]: ".format(count))

    line = ""

    while True:
        char = getchar()
        
        if char in "\x04":
            char = ""
            if not line:
                sys.stdout.write("\nkthankxbye!\n")
                sys.exit(0)
            else:
                sys.stdout.write("\nKeyboardInterrupt\n")
                return
        elif char in "\x1b":

            arrow = sys.stdin.read(2)

            if history:
                if arrow == "[A" and index < len(history): #check that we are within bounds
                    if index == 0 and line:
                        history.append(line)
                        index += 1

                    line = list(reversed(history))[index] # Ugly hack: to get the correct order we need to reverse the list
                    sys.stdout.write("\nIn [{}]: {}".format(count, line))
                    index += 1 
                elif arrow == "[B" and index > 0: ##bounds check
                    index -= 1
                    line = list(reversed(history))[index-1]
                    sys.stdout.write("\nIn [{}]: {}".format(count, line))
            continue
        
        sys.stdout.write(char)
                    
        if char in "\r\n": ##newline
            sys.stdout.write("\n" + feedline(line)+ "\n") #off to feedline
            if line:
                count = count + 1
            return
        
        line += char

if __name__ == "__main__":
    print "Welcome to mypython!"
    while True:
        prompt()
