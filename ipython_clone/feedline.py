import sys, subprocess, inspect
from StringIO import StringIO

namespace = vars().copy()
history = [] #history is stored in a list

def feedline(code):
    """
    Executes a line of (my)python code

    Args:
        code: A string representation of python code

    Returns:
        The result of the execution
    """
    if code: #if not empty statement
        history.append(code)

    out = ""

    if code.startswith("!"):
        code = code[1:] #strip off !
        
        proc = subprocess.Popen(code.split(), stdout=subprocess.PIPE)
        (out, err) = proc.communicate()
                
    elif code.endswith("?"):
        code = code[:-1] #strip off question mark
        try:
            obj = compile(code, "<string>", 'eval') #compile
            obj = eval(obj, namespace) #result
            out = inspect.getdoc(obj) #get docs!
        except:
            out = "Error: {}\n".format(sys.exc_info()[1]) ##else print errors
        
    elif code.startswith("%save"):
        if  len(code.split()) < 2: ## check if code consists of at least two words (i.e. %save filename)
            out = "Error: Missing filename"
        else:
            with open(code.split()[1], "w") as fp: ## file io
                for item in history:
                    fp.write("{}\n".format(item))
            out = "saved to " + code.split()[1]
    else:
        try:
            out = str(eval(code, namespace)) + "\n" #first try eval
        except:
            try: #if fail, we try exec
                oldio, sys.stdout = sys.stdout, StringIO() #IO trickery, redirect streams
                exec(code, namespace) 
                out = sys.stdout.getvalue() ##retrieve value
                sys.stdout = oldio ## back to normal
            except:
                sys.stdout = oldio
                out = "Error: {}\n".format(sys.exc_info()[1])
            
    return out

