from flask import Flask, render_template, request, url_for

import feedline

app = Flask(__name__)

output = []

@app.route("/")
def hello():
    return render_template("test.html", output=zip(feedline.history,output))

@app.route("/handle_cmd", methods=["POST"])
def handle():
    cmd = request.form["cmd"]
    if cmd:
        output.append(feedline.feedline(cmd))
    return hello()

if __name__ == "__main__":
    app.run()
